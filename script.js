function filterBy(arr, type){
    let newArr;
    switch(type){
        case "string":
            newArr = arr.filter(function(el) { return typeof el !== "string" });
            break;
        case "number":
            newArr = arr.filter(function(el) { return typeof el !== "number" });
            break;
        case "boolean":
            newArr = arr.filter(function(el) { return typeof el !== "boolean" });
            break;
        case "object":
            newArr = arr.filter(function(el) { return typeof el !== "object" });
            break;
        case "symbol":
            newArr = arr.filter(function(el) { return typeof el !== "symbol" });
            break;
        case "undefined":
            newArr = arr.filter(function(el) { return typeof el !== "undefined" });
            break;
        case "function":
            newArr = arr.filter(function(el) { return typeof el !== "function" });
            break;
        case "null":
            newArr = arr.filter(function(el) { return el !== null });
            break;
        case "array":
            newArr = arr.filter(function(el) { 
                for (let i = 0; i < arr.length; i++)
                {
                    if (Array.isArray(arr[i]) == false)
                        return el;
                    }
                });
            break;
        default:
            break;
    }
    return newArr;  
}


let array = [1,2,"meow", undefined, null, 'lev0', `prav0`, [1,2], ["a", 'b'], Symbol("id"), {a:10}, () => { console.log("I really like corn") }];
let newArray = [];
newArray[0] = filterBy(array, "string");
newArray[1] = filterBy(array, "number");
newArray[2] = filterBy(array, "boolean");
newArray[3] = filterBy(array, "object");
newArray[4] = filterBy(array, "symbol");
newArray[5] = filterBy(array, "undefined");
newArray[6] = filterBy(array, "function"); // я знаю, що функція не тип даних, але мені захотілось зробити і це
newArray[7] = filterBy(array, "null");
newArray[8] = filterBy(array, "array"); // я знаю, що масив не тип даних, але мені захотілось зробити і це

for (let i = 0; i < newArray.length; i++){
    let str = ["string", "number", "boolean", "object", "symbol", "undefined", "function", "null", "array"];
    console.log(`Without ${str[i]}: `);
    console.log(newArray[i]);
}

